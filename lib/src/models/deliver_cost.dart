class DeliveryCost {
  int id;
  int initialCost;
  int perKm;
  int minimumCharges;

  DeliveryCost({this.id, this.initialCost, this.perKm, this.minimumCharges});

  DeliveryCost.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    initialCost = json['initial_cost'];
    perKm = json['per_km'];
    minimumCharges = json['minimum_charges'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['initial_cost'] = this.initialCost;
    data['per_km'] = this.perKm;
    data['minimum_charges'] = this.minimumCharges;
    return data;
  }
}