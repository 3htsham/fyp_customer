import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:markets/src/elements/CircularLoadingWidget.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

import '../../generated/l10n.dart';
import '../controllers/delivery_pickup_controller.dart';
import '../elements/CartBottomDetailsWidget.dart';
import '../elements/DeliveryAddressDialog.dart';
import '../elements/DeliveryAddressesItemWidget.dart';
import '../elements/NotDeliverableAddressesItemWidget.dart';
import '../elements/PickUpMethodItemWidget.dart';
import '../elements/ShoppingCartButtonWidget.dart';
import '../helpers/helper.dart';
import '../models/address.dart';
import '../models/payment_method.dart';
import '../models/route_argument.dart';
import '../repository/settings_repository.dart' as settingsRepo;

class DeliveryPickupWidget extends StatefulWidget {
  final RouteArgument routeArgument;

  DeliveryPickupWidget({Key key, this.routeArgument}) : super(key: key);

  @override
  _DeliveryPickupWidgetState createState() => _DeliveryPickupWidgetState();
}

class _DeliveryPickupWidgetState extends StateMVC<DeliveryPickupWidget> {
  DeliveryPickupController _con;

  _DeliveryPickupWidgetState() : super(DeliveryPickupController()) {
    _con = controller;
  }

  @override
  Widget build(BuildContext context) {
    if (_con.list == null) {
      _con.list = new PaymentMethodList(context);
//      widget.pickup = widget.list.pickupList.elementAt(0);
//      widget.delivery = widget.list.pickupList.elementAt(1);
    }
    return Scaffold(
      key: _con.scaffoldKey,
      bottomNavigationBar: CartBottomDetailsWidget(con: _con),
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        title: Text(
          S.of(context).delivery_or_pickup,
          style: Theme.of(context).textTheme.headline6.merge(TextStyle(letterSpacing: 1.3)),
        ),
        actions: <Widget>[
          new ShoppingCartButtonWidget(iconColor: Theme.of(context).hintColor, labelColor: Theme.of(context).accentColor),
        ],
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.symmetric(vertical: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 10),
              child: ListTile(
                contentPadding: EdgeInsets.symmetric(vertical: 0),
                leading: Icon(
                  Icons.domain,
                  color: Theme.of(context).hintColor,
                ),
                title: Text(
                  S.of(context).pickup,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: Theme.of(context).textTheme.headline4,
                ),
                subtitle: Text(
                  S.of(context).pickup_your_product_from_the_market,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: Theme.of(context).textTheme.caption,
                ),
              ),
            ),
            PickUpMethodItem(
                paymentMethod: _con.getPickUpMethod(),
                onPressed: (paymentMethod) {
                  _con.togglePickUp();
                }),

            Padding(
              padding: const EdgeInsets.only(top: 20, bottom: 10, left: 20, right: 10),
              child: ListTile(
                contentPadding: EdgeInsets.symmetric(vertical: 0),
                leading: Icon(
                  Icons.airport_shuttle_sharp,
                  color: Theme.of(context).accentColor,
                ),
                title: Text(
                  'Premium Delivery',
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: Theme.of(context).textTheme.headline4,
                ),
                subtitle: Text(
                  'If you want to get delivery via car, you can contact us (after placing order) at: +92 331 1234567',
                  maxLines: 3,
                  overflow: TextOverflow.ellipsis,
                  style: Theme.of(context).textTheme.caption,
                ),
              ),
            ),

            Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 20, bottom: 10, left: 20, right: 10),
                  child: ListTile(
                    contentPadding: EdgeInsets.symmetric(vertical: 0),
                    leading: Icon(
                      Icons.map,
                      color: Theme.of(context).hintColor,
                    ),
                    title: Text(
                      S.of(context).delivery,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: Theme.of(context).textTheme.headline4,
                    ),
                    subtitle:
                    // _con.carts.isNotEmpty && Helper.canDelivery(_con.carts[0].product.market, carts: _con.carts)
                    //     ?
                    Text(
                            S.of(context).click_to_confirm_your_address_and_pay_or_long_press,
                            maxLines: 3,
                            overflow: TextOverflow.ellipsis,
                            style: Theme.of(context).textTheme.caption,
                          )
                        // : Text(
                        //     S.of(context).deliveryMethodNotAllowed,
                        //     maxLines: 3,
                        //     overflow: TextOverflow.ellipsis,
                        //     style: Theme.of(context).textTheme.caption,
                        //   ),
                  ),
                ),
                // _con.carts.isNotEmpty && Helper.canDelivery(_con.carts[0].product.market, carts: _con.carts)
                _con.carts.isEmpty
                ? CircularLoadingWidget(height: 40,)
                : _con.carts.isNotEmpty && settingsRepo.deliveryAddress.value?.address != null
                    ? DeliveryAddressesItemWidget(
                        paymentMethod: _con.getDeliveryMethod(),
                        address: _con.deliveryAddress,
                        onPressed: (Address _address) {
                          if (_con.deliveryAddress.id == null || _con.deliveryAddress.id == 'null') {
                            DeliveryAddressDialog(
                              context: context,
                              address: _address,
                              onChanged: (Address _address) {
                                _con.addAddress(_address);
                              },
                            );
                          } else {
                            _con.toggleDelivery();
                          }
                        },
                        onLongPress: (Address _address) {
                          DeliveryAddressDialog(
                            context: context,
                            address: _address,
                            onChanged: (Address _address) {
                              _con.updateAddress(_address);
                            },
                          );
                        },
                      )
                    : NotDeliverableAddressesItemWidget(),

                (_con.carts.isNotEmpty && settingsRepo.deliveryAddress.value?.address != null) || (_con.carts.isNotEmpty && _con.getPickUpMethod().selected)
                ? Padding(
                  padding: const EdgeInsets.only(top: 20, bottom: 10, left: 20, right: 10),
                  child: ListTile(
                    onTap: (){
                      if(settingsRepo.scheduledDateAndTime.length > 0) {
                        setState((){
                          settingsRepo.scheduledDateAndTime = '';
                        });
                      } else {
                        DatePicker.showDateTimePicker(context,
                            showTitleActions: true,
                            minTime: DateTime.now(),
                            maxTime: DateTime.now().add(Duration(days: 90),),
                            theme: DatePickerTheme(
                                doneStyle: Theme
                                    .of(context)
                                    .textTheme
                                    .subtitle1
                                    .copyWith(
                                    color: Theme
                                        .of(context)
                                        .accentColor
                                ),
                                cancelStyle: Theme
                                    .of(context)
                                    .textTheme
                                    .subtitle1
                                    .copyWith(
                                    color: Theme
                                        .of(context)
                                        .hintColor
                                ),
                                backgroundColor: Theme
                                    .of(context)
                                    .scaffoldBackgroundColor,
                                itemStyle: Theme
                                    .of(context)
                                    .textTheme
                                    .subtitle1,
                                headerColor: Theme
                                    .of(context)
                                    .primaryColor
                            ),
                            onChanged: (date) {
                              // print('change $date');
                            },
                            onConfirm: (date) {
                              print('confirm $date');
                              //yyyy-MM-dd HH:mm:ss.000
                              debugPrint(date.toString());
                              setState((){
                                settingsRepo.scheduledDateAndTime = date.toString();
                              });
                            },
                            currentTime: DateTime.now(),
                            locale: LocaleType.en
                        );
                      }
                    },
                    contentPadding: EdgeInsets.symmetric(vertical: 0),
                    leading: Icon(
                      Icons.access_time,
                      color: Theme.of(context).hintColor,
                    ),
                    title: Text(
                      'Schedule Order',
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: Theme.of(context).textTheme.headline4,
                    ),
                    subtitle: Text(
                      settingsRepo.scheduledDateAndTime.length > 0
                      ? '${settingsRepo.scheduledDateAndTime}'
                      : 'Pre book your order now',
                      maxLines: 3,
                      overflow: TextOverflow.ellipsis,
                      style: Theme.of(context).textTheme.caption,
                    ),
                    trailing: Icon(
                      settingsRepo.scheduledDateAndTime.length > 0
                      ? Icons.check_box
                      : Icons.check_box_outline_blank,
                      color: Theme.of(context).accentColor,
                    ),
                  ),
                )
                    : SizedBox(),
              ],
            )
          ],
        ),
      ),
    );
  }

  void _displayDatetimePickerDialog() {

  }

}
