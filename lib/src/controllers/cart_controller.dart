import 'dart:math';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import '../../generated/l10n.dart';
import '../models/cart.dart';
import '../models/coupon.dart';
import '../models/market.dart';
import '../repository/cart_repository.dart';
import '../repository/coupon_repository.dart';
import '../repository/settings_repository.dart';
import '../repository/user_repository.dart';
import '../repository/settings_repository.dart' as settingsRepo;

class CartController extends ControllerMVC {
  List<Cart> carts = <Cart>[];
  double taxAmount = 0.0;
  int cartCount = 0;
  double subTotal = 0.0;
  double total = 0.0;
  GlobalKey<ScaffoldState> scaffoldKey;
  Market market;
  double deliveryFee = 0.0;

  CartController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
  }

  void listenForCarts({String message}) async {
    carts.clear();
    final Stream<Cart> stream = await getCart();
    stream.listen((Cart _cart) {
      if (!carts.contains(_cart)) {
        setState(() {
          coupon = _cart.product.applyCoupon(coupon);
          carts.add(_cart);
          market = _cart.product.market;
        });
      }
    }, onError: (a) {
      print(a);
      scaffoldKey?.currentState?.showSnackBar(SnackBar(
        content: Text(S.of(scaffoldKey?.currentContext).verify_your_internet_connection),
      ));
    }, onDone: () {
      if (carts.isNotEmpty) {
        calculateSubtotal();
      }
      if (message != null) {
        scaffoldKey?.currentState?.showSnackBar(SnackBar(
          content: Text(message),
        ));
      }
      onLoadingCartDone();
    });
  }


  void onLoadingCartDone() {}

  void listenForCartsCount({String message}) async {
    final Stream<int> stream = await getCartCount();
    stream.listen((int _count) {
      setState(() {
        this.cartCount = _count;
      });
    }, onError: (a) {
      print(a);
      scaffoldKey?.currentState?.showSnackBar(SnackBar(
        content: Text(S.of(scaffoldKey?.currentContext).verify_your_internet_connection),
      ));
    });
  }

  Future<void> refreshCarts() async {
    setState(() {
      carts = [];
    });
    listenForCarts(message: S.of(scaffoldKey?.currentContext).carts_refreshed_successfuly);
  }

  void removeFromCart(Cart _cart) async {
    setState(() {
      this.carts.remove(_cart);
    });
    removeCart(_cart).then((value) {
      calculateSubtotal();
      scaffoldKey?.currentState?.showSnackBar(SnackBar(
        content: Text(S.of(scaffoldKey?.currentContext).the_product_was_removed_from_your_cart(_cart.product.name)),
      ));
    });
  }

  void calculateSubtotal() async {
    double cartPrice = 0;
    subTotal = 0;
    carts.forEach((cart) {
      cartPrice = cart.product.price;
      cart.options.forEach((element) {
        cartPrice += element.price;
      });
      cartPrice *= cart.quantity;
      subTotal += cartPrice;
    });
    calculateCharges();
    // if (Helper.canDelivery(carts[0].product.market, carts: carts)) {
    //   deliveryFee = carts[0].product.market.deliveryFee;
    // }
    taxAmount = (subTotal + deliveryFee) * carts[0].product.market.defaultTax / 100;
    total = subTotal + taxAmount + deliveryFee;
    setState(() {});
  }

  void calculateCharges() {
    var address = settingsRepo.deliveryAddress.value;
    print('address: ${settingsRepo.deliveryAddress.value.toMap()}');
    print('market: ${this.market.toMap()}');
    if(address?.address != null) {
      //TODO: 1 - Initial Charges for all deliveries are in settingsRepo.deliveryCost.initialCost (will must be added anyway)
      //TODO: 2 - Calculate the distance in KMs then, user will be charged (settingsRepo.deliveryCost.perKm) for every KM
      //TODO: If the sum of Step 1 & Step 2 is < settingsRepo.deliveryCost.minimumCharges, then set DeliveryCharges to settingsRepo.deliveryCost.minimumCharges
      final _initCharges = settingsRepo.deliveryCost.initialCost;
      final distance = calculateDistance(
          double.parse(this.market.latitude), double.parse(this.market.longitude),
          settingsRepo.deliveryAddress.value.latitude,
          settingsRepo.deliveryAddress.value.longitude
      );
      final _chargesForDistance =  distance * settingsRepo.deliveryCost.perKm;
      final _totalCharges = _initCharges + _chargesForDistance;

      if(_totalCharges < settingsRepo.deliveryCost.minimumCharges) {
        setState((){
          deliveryFee = settingsRepo.deliveryCost.minimumCharges.toDouble();});
      } else {
        setState((){
          deliveryFee = _totalCharges;});
      }
    }
  }
  //lat1, long1 == Market's Location
  //lat2, long2 == User's location
  double calculateDistance(lat1, lon1, lat2, lon2){
    var p = 0.017453292519943295;
    var c = cos;
    var a = 0.5 - c((lat2 - lat1) * p)/2 +
        c(lat1 * p) * c(lat2 * p) *
            (1 - c((lon2 - lon1) * p))/2;
    return 12742 * asin(sqrt(a));
  }

  void doApplyCoupon(String code, {String message}) async {
    coupon = new Coupon.fromJSON({"code": code, "valid": null});
    final Stream<Coupon> stream = await verifyCoupon(code);
    stream.listen((Coupon _coupon) async {
      coupon = _coupon;
    }, onError: (a) {
      print(a);
      scaffoldKey?.currentState?.showSnackBar(SnackBar(
        content: Text(S.of( scaffoldKey?.currentContext).verify_your_internet_connection),
      ));
    }, onDone: () {
      listenForCarts();
    });
  }

  incrementQuantity(Cart cart) {
    if (cart.quantity <= 99) {
      ++cart.quantity;
      updateCart(cart);
      calculateSubtotal();
    }
  }

  decrementQuantity(Cart cart) {
    if (cart.quantity > 1) {
      --cart.quantity;
      updateCart(cart);
      calculateSubtotal();
    }
  }

  void goCheckout(BuildContext context) {
    if (!currentUser.value.profileCompleted()) {
      scaffoldKey?.currentState?.showSnackBar(SnackBar(
        content: Text(S.of(scaffoldKey?.currentContext).completeYourProfileDetailsToContinue),
        action: SnackBarAction(
          label: S.of(scaffoldKey?.currentContext).settings,
          textColor: Theme.of(scaffoldKey?.currentContext).accentColor,
          onPressed: () {
            Navigator.of(scaffoldKey?.currentContext).pushNamed('/Settings');
          },
        ),
      ));
    } else {
      if (carts[0].product.market.closed) {
        scaffoldKey?.currentState?.showSnackBar(SnackBar(
          content: Text(S.of(scaffoldKey?.currentContext).this_market_is_closed_),
        ));
      } else {
        Navigator.of(scaffoldKey?.currentContext).pushNamed('/DeliveryPickup');
      }
    }
  }

  Color getCouponIconColor() {
    if (coupon?.valid == true) {
      return Colors.green;
    } else if (coupon?.valid == false) {
      return Colors.redAccent;
    }
    return Theme.of(scaffoldKey?.currentContext).focusColor.withOpacity(0.7);
  }
}
